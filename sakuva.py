#!/usr/bin/env python3

'''
    Retrieve a photo from the Finnish Wartime Photograph Archive
    (and upload it to Wikimedia Commons).

    Usage: ./sakuva.py <ID> [<filename override>]
           ./sakuva.py <ID> --only-download

    Commons filename (without extension) may be specified in command line.

    @author Alexei Soloviev

    This program is free software; you can redistribute it and/or modify it under the
    terms of the GNU General Public License as published by the Free Software
    Foundation; either version 2 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
    PARTICULAR PURPOSE. See the GNU General Public License for more details.
'''

import sys
import tempfile
import requests
import re
import os.path
from iptcinfo3 import IPTCInfo

# SA-Kuva ID (may be specified at command line)
ID = "46151"

# Commons auth data (DON'T FORGET TO SPECIFY YOURS):
LOGIN = "pupkin"
PASSWORD = "qwerty"
# if login or password is False or Null the retrieved image won't be uploaded to WikiCommons


OUT_DIRECTORY = tempfile.gettempdir()

if len(sys.argv)<2:
    print("Usage: %s <ID>" % sys.argv[0])
    sys.exit(0)

ID = sys.argv[1]

if len(sys.argv)>2 and sys.argv[2] == '--only-download':
    sys.argv[2] = ''
    LOGIN = False

print("Loading photo with ID %s into directory %s..." % (ID, OUT_DIRECTORY))

resp = requests.get("http://sa-kuva.fi/neo?tem=webneoeng&lang=ENG&startdate=19390101&enddate=19451231&publication=&department=%5Bui_texts.departmentplaceholder%5D&xsearch_content="+ID);
if resp.status_code != 200:
    print("Unsucessful response!")
    sys.exit(1)
res = re.search(r"onload=[^/]+([^']+)", resp.text)
if not res:
    print("Incorrect response!")
    sys.exit(2)
search_photo = res.group(1)

resp = requests.get("http://sa-kuva.fi"+search_photo)
if resp.status_code != 200:
    print("Incorrect response!")
    sys.exit(3)
res1 = re.search(r"doc_id=([^&]+)", resp.text)
if not res1:
    print("Incorrect response!")
    sys.exit(4)
res2 = re.search(r"from=([^&]+)", resp.text)
if not res2:
    print("Incorrect response!")
    sys.exit(5)

image_resp = requests.get("http://sa-kuva.fi/neo2?tem=webneo_image_download&lang=ENG&id=%s&archive=&name=%s" % (res1.group(1), ID))
image_fn = os.path.join(OUT_DIRECTORY, ID+".jpg")
f = open(image_fn, "wb")
f.write(image_resp.content)
f.close()

info = {'id':ID, 'author':'unknown', 'source':'SA-Kuva', 'description':'', 'place':'', 'date': '', 'filename':' '.join(sys.argv[2:])}
img_metadata = IPTCInfo(image_fn)
try:
    description = img_metadata['caption/abstract'].decode('utf8').strip()
except:
    print('description: assuming cp1252')
    description = img_metadata['caption/abstract'].decode('cp1252').strip()
info['description'] = description[0:-1] if description[-1] == '.' else description
photodate = img_metadata['date created'].decode().strip()
info['date'] = '%s-%s-%s' % (photodate[0:4], photodate[4:6], photodate[6:8])
try:
    info['place'] = img_metadata['city'].decode('utf8').strip()
except:
    print('place: assuming cp1252')
    info['place'] = img_metadata['city'].decode('cp1252').strip()
try:
    info['author'] = img_metadata['by-line'].decode('utf8').strip()
except:
    print('author: assuming cp1252')
    info['author'] = img_metadata['by-line'].decode('cp1252').strip()
if not info['filename']:
    info['filename'] = "%s, %s, SAKuva-%s" % (info['place'], info['description'], ID)
info['filename'] += '.jpg'
print(info)

text = '''=={{int:filedesc}}==
{{Information
|description={{fi|1=%s, %s}}
|date=%s
|source=[http://sa-kuva.fi/neo?tem=webneo_image_large&lang=ENG&imgid=%s&docid=%s&archive= SA-Kuva ID:%s]
|author=%s
|permission=
|other versions=
}}

=={{int:license-header}}==

{{PD-Finland50}}
[[Category:SA-kuva images with watermarks]]''' % (info['place'], info['description'], info['date'], res1.group(1), res2.group(1), ID, info['author'])
print(text)

if not LOGIN or not PASSWORD:
    sys.exit(0)

print("READY TO UPLOAD...")

BASE_URL = "https://commons.wikimedia.org/w/api.php"
s = requests.Session()

#query auth manager
resp = s.get(BASE_URL+"?action=query&format=json&meta=authmanagerinfo&amirequestsfor=login")

#obtain LOGIN-token
resp = s.get(BASE_URL+"?action=query&format=json&meta=tokens&type=login")
login_resp = resp.json()
if 'tokens' in login_resp['query'] and 'logintoken' in login_resp['query']['tokens']:
    login_token = login_resp['query']['tokens']['logintoken']
else:
    print("Cannot obtain login token: "+resp.text)
    sys.exit(7)

#auth
resp = s.post(BASE_URL+"?action=clientlogin&format=json", data={"username":LOGIN, "password":PASSWORD, "logintoken":login_token, "rememberMe":"1", "loginreturnurl":BASE_URL})
auth_resp = resp.json()
if 'status' in auth_resp['clientlogin'] and auth_resp['clientlogin']['status'] == 'PASS':
    pass
else:
    print("Login failed: "+resp.text)
    sys.exit(8)

# obtain CSRF-token
resp = s.get(BASE_URL+"?action=query&format=json&meta=tokens")
csrf_resp = resp.json()
if 'tokens' in csrf_resp['query'] and 'csrftoken' in csrf_resp['query']['tokens']:
    csrf_token = csrf_resp['query']['tokens']['csrftoken']
else:
    print("Cannot obtain CSRF token: "+resp.text)
    sys.exit(9)

#upload file
resp = s.post(BASE_URL+"?action=upload&format=json&ignorewarnings=true", files={'file': image_resp.content},
    data={"filename": info['filename'],
    "token": csrf_token,
    "comment": "SA-Kuva "+ID,
    "text": text})
result = resp.json()
if 'result' in result['upload'] and result['upload']['result'] == 'Success':
    print("Uploaded: "+result['upload']['filename'])
else:
    print("Upload failed: "+resp.text)
    sys.exit(10)
