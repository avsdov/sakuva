# sakuva
    Retrieve a photo from the Finnish Wartime Photograph Archive
    (and upload it to Wikimedia Commons).

    Usage: ./sakuva.py <ID> [<filename override>]
           ./sakuva.py <ID> --only-download

    Commons filename (without extension) may be specified in command line.

    @author Alexei Soloviev

    This program is free software; you can redistribute it and/or modify it under the
    terms of the GNU General Public License as published by the Free Software
    Foundation; either version 2 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
    PARTICULAR PURPOSE. See the GNU General Public License for more details.


    REQUIREMENTS:

        pip3 install IPTCInfo3

    CHANGES:

        2020-08-13: Fetch metadata from IPTC.
        2020-08-18: Command line parameters (--only-download and Commons filename).

# pastvu

    Retrieve a photo from the PastVu Project
    (and upload it to Wikimedia Commons).

    Usage: ./pastvu.py <ID> [<filename override>]
           ./pastvu.py <ID> --only-download

    Commons filename (without extension) may be specified in command line.

    REQUIREMENTS:
        * IPTCInfo3
        * PIL (Python Imaging Library)
