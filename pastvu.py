#!/usr/bin/env python3

'''
    Retrieve a photo from the PastVu Project
    (and upload it to Wikimedia Commons).

    Usage: ./pastvu.py <ID> [<filename override>]
           ./pastvu.py <ID> --only-download

    Commons filename (without extension) may be specified in command line.

    @author Alexei Soloviev

    This program is free software; you can redistribute it and/or modify it under the
    terms of the GNU General Public License as published by the Free Software
    Foundation; either version 2 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY
    WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
    PARTICULAR PURPOSE. See the GNU General Public License for more details.
'''

import sys
import tempfile
import requests
import re
import os.path
import json
from PIL import Image
from iptcinfo3 import IPTCInfo

# PastVu ID (may be specified at command line)
ID = "46151"

# Commons auth data (DON'T FORGET TO SPECIFY YOURS):
LOGIN = "pupkin"
PASSWORD = "qwerty"
# if login or password is False or Null the retrieved image won't be uploaded to WikiCommons

DIRECTIONS = {'n':0, 'ne':45, 'e':90, 'se':135, 's':180, 'sw':225, 'w':270, 'nw':315}

OUT_DIRECTORY = tempfile.gettempdir()

if len(sys.argv)<2:
    print("Usage: %s <ID> [<filename override>]" % sys.argv[0])
    print("       %s <ID> --only-download" % sys.argv[0])
    sys.exit(0)

ID = sys.argv[1]

if len(sys.argv)>2 and sys.argv[2] == '--only-download':
    sys.argv[2] = ''
    LOGIN = False

print("Loading photo with ID %s into directory %s..." % (ID, OUT_DIRECTORY))

resp = requests.get("https://pastvu.com/p/"+ID)
if resp.status_code != 200:
    print("Unsucessful response!")
    sys.exit(1)
js = re.search(r"var\s+init\s*=\s*({.*})[;]*</script>", resp.text)
if not js:
    print("Incorrect response!")
    sys.exit(2)
lazy_json_str = js.group(1).replace('settings:', '"settings":')
lazy_json_str = lazy_json_str.replace('user:', '"user":')
lazy_json_str = lazy_json_str.replace('photo:', '"photo":')
pastvu = json.loads(lazy_json_str)
# print(pastvu)

info = {'id':ID, 'author':'unknown', 'source':"https://pastvu.com/p/"+ID, 'description':'', 'place':'', 'date': '', 'filename':' '.join(sys.argv[2:])}
image_resp = requests.get("https://pastvu.com/_p/a/"+pastvu['photo']['photo']['file'])
image_fn = os.path.join(OUT_DIRECTORY, ID+".jpg")
f = open(image_fn, "wb")
f.write(image_resp.content)
f.close()
img_metadata = IPTCInfo(image_fn)
img_obj = Image.open(image_fn)
cropped = img_obj.crop((0,0,pastvu['photo']['photo']['w'],pastvu['photo']['photo']['h']))
img_obj.close()
f = open(image_fn, "wb")
cropped.save(f)
cropped.close()
f.close()
img_metadata.save()
try:
    description = img_metadata['caption/abstract'].decode('utf8').strip()
except:
    print('description: assuming cp1252')
    try:
        description = img_metadata['caption/abstract'].decode('cp1252').strip()
    except:
        description = pastvu['photo']['photo']['title']
info['description'] = description[0:-1] if description[-1] == '.' else description
try:
    photodate = img_metadata['date created'].decode().strip()
    info['date'] = '%s-%s-%s' % (photodate[0:4], photodate[4:6], photodate[6:8])
except:
    info['date'] = pastvu['photo']['photo']['year']
try:
    info['place'] = img_metadata['city'].decode('utf8').strip()
    if not info['filename']:
        info['filename'] = "%s, %s" % (info['place'], info['description']) 
except:
    print('place: assuming cp1252')
    try:
        info['place'] = img_metadata['city'].decode('cp1252').strip()
        if not info['filename']:
            info['filename'] = "%s, %s" % (info['place'], info['description']) 
    except:
        if not info['filename']:
            info['filename'] = "%s" % (info['description']) 
        info['place'] = ''
try:
    info['author'] = pastvu['photo']['photo']['author']
except:
    info['author'] = 'unknown'
info['filename'] += '.jpg'
info['source'] = "https://pastvu.com/p/"+ID
info['dir'] = pastvu['photo']['photo']['dir']
info['geo'] = pastvu['photo']['photo']['geo']
print(info)

text = ''
if 'sa-kuva' in pastvu['photo']['photo']['source']:
    text = '[[Category:SA-kuva images with watermarks]]'
    info['source'] += ', ' + pastvu['photo']['photo']['source']
text = '''=={{int:filedesc}}==
{{Information
|description=%s
|date=%s
|source=%s
|author=%s
|permission=
|other versions=
}}
{{Location|%s|%s|heading:%s}}

=={{int:license-header}}==

{{PD-Finland50}}
''' % (info['description'] if not info['place'] else info['place']+', '+info['description'], 
    info['date'], info['source'], info['author'], info['geo'][0], info['geo'][1], DIRECTIONS[info['dir']]) + text
print(text)

if not LOGIN or not PASSWORD:
    sys.exit(0)

print("READY TO UPLOAD...")

BASE_URL = "https://commons.wikimedia.org/w/api.php"
s = requests.Session()

#query auth manager
resp = s.get(BASE_URL+"?action=query&format=json&meta=authmanagerinfo&amirequestsfor=login")

#obtain LOGIN-token
resp = s.get(BASE_URL+"?action=query&format=json&meta=tokens&type=login")
login_resp = resp.json()
if 'tokens' in login_resp['query'] and 'logintoken' in login_resp['query']['tokens']:
    login_token = login_resp['query']['tokens']['logintoken']
else:
    print("Cannot obtain login token: "+resp.text)
    sys.exit(7)

#auth
resp = s.post(BASE_URL+"?action=clientlogin&format=json", data={"username":LOGIN, "password":PASSWORD, "logintoken":login_token, "rememberMe":"1", "loginreturnurl":BASE_URL})
auth_resp = resp.json()
if 'status' in auth_resp['clientlogin'] and auth_resp['clientlogin']['status'] == 'PASS':
    pass
else:
    print("Login failed: "+resp.text)
    sys.exit(8)

# obtain CSRF-token
resp = s.get(BASE_URL+"?action=query&format=json&meta=tokens")
csrf_resp = resp.json()
if 'tokens' in csrf_resp['query'] and 'csrftoken' in csrf_resp['query']['tokens']:
    csrf_token = csrf_resp['query']['tokens']['csrftoken']
else:
    print("Cannot obtain CSRF token: "+resp.text)
    sys.exit(9)

#upload file
resp = s.post(BASE_URL+"?action=upload&format=json&ignorewarnings=true", files={'file': open(image_fn, 'rb')},
    data={"filename": info['filename'],
    "token": csrf_token,
    "comment": "PastVu "+ID,
    "text": text})
result = resp.json()
if 'result' in result['upload'] and result['upload']['result'] == 'Success':
    print("Uploaded: "+result['upload']['filename'])
else:
    print("Upload failed: "+resp.text)
    sys.exit(10)
